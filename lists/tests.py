from django.test import TestCase

from lists.models import Item, List


class ItemAndListModelTests(TestCase):
    def test_saving_and_retreiving_items(self):
        list_ = List()
        list_.save()

        first_item = Item()
        first_item.text = "First list item"
        first_item.list = list_
        first_item.save()

        second_item = Item()
        second_item.text = "Second item"
        second_item.list = list_
        second_item.save()

        saved_list = List.objects.first()

        self.assertEqual(saved_list, list_)

        saved_items = Item.objects.all()

        self.assertEqual(saved_items.count(), 2)

        self.assertEqual(saved_items[0].text, "First list item")
        self.assertEqual(saved_items[0].list, list_)
        self.assertEqual(saved_items[1].text, "Second item")
        self.assertEqual(saved_items[1].list, list_)


class HomePageTests(TestCase):

    def test_home_page_returns_correct_html(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response, 'home.html')


class ListViewTest(TestCase):
    def test_displays_all_list_items(self):
        list_ = List.objects.create()
        item_1 = Item.objects.create(text="First item", list=list_)
        item_2 = Item.objects.create(text="Second item", list=list_)

        response = self.client.get("/lists/the-only-list/")

        self.assertContains(response, "First item")
        self.assertContains(response, "Second item")

    def test_uses_list_template(self):
        response = self.client.get("/lists/the-only-list/")
        self.assertTemplateUsed(response, "list.html")


class NewListTest(TestCase):

    def test_should_redirect_after_post_request(self):
        response = self.client.post('/lists/new', data={'item_text': 'A new list item'})
        self.assertRedirects(response, "/lists/the-only-list/")

    def test_should_save_item_to_database_on_post_request(self):
        response = self.client.post('/lists/new', data={'item_text': 'A new list item'})

        new_item = Item.objects.first()

        self.assertEqual(Item.objects.all().count(), 1)
        self.assertEqual(new_item.text, "A new list item")
